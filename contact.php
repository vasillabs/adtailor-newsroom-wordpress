<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>

<div class="main-content container">
		<div class="wrapper contact-wrapper">
			<h2>Company Info</h2>
			<div class="contact-main">
				<p>Adtailor is the world's first ever big-data audience profiler. The company was founded back in 2014 and has been working ever since to provide an innovative audience profiling software that works in real time on any given online business platform.</p>
				<p>Adtailor's solution uses third party official information sources, 100% anonymously compiled; analyzes a number of different criteria in real time and alters what every different visitor sees on our client’s website.</p>
				<p>Adtailor is here to help other businesses of all industries improve their results in terms of engagement, conversions and sales.</p>
				<img class="img-responsive team-image" src="<?php bloginfo("stylesheet_directory") ?>/img/team.jpg" alt="Adtailor team">
			</div>
			
		</div>
	</div> <!-- end main content -->
<?php get_footer(); ?>
