<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php wp_title( 'Adtailor |', true, 'left' ); ?></title>
	
	<!--  Styles  -->
	<!--<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">-->
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<!-- Add favicons -->	

	<link rel="shortcut icon" type="image/png" href="<?php bloginfo("stylesheet_directory"); ?>/img/favicon_16.png" />
       <link rel="shortcut icon" type="image/png" href="<?php bloginfo("stylesheet_directory"); ?>/img/favicon_32.png" />
       <link rel="shortcut icon" type="image/png" href="<?php bloginfo("stylesheet_directory"); ?>/img/favicon_48.png" />

	<?php wp_head(); ?>
</head>
<body>
	
	<header class="header">
		<div class="container">
			<div class="row header-top">
				<h1 class="col-md-3"><a href="<?php echo home_url(); ?>">Adtailor newsroom</a></h1>
				<span class="col-md-1  col-md-offset-8 video-play">Play Video</span>
			</div>
		</div>	
		
		
<!--		<nav class="navigation">
				<ul class="container">
					<li><a href="news.html">News</a></li>
					<li><a href="companyinfo.html">Company Info</a></li>
					<li><a href="media.html">Media Gallery</a></li>
				</ul>
		</nav>
-->
		<?php wp_nav_menu(array(
			'theme_location'	=>	'top-menu',
			'container'			=>	'nav',
			'container_class'	=>	'navigation',
			'menu_class'		=>	'container'
		)); 
		
		$children = wp_get_nav_menu_items( 'Top Menu' );
		
		?>
	</header>
	
