<?php remove_filter ('the_excerpt', 'wpautop'); ?>
<?php get_header();?>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	
	<div class="main-content container">	
		<div class="wrapper">
			<div class="single-post-wrapper" id="post-<?php the_ID(); ?>">
				<div class="single-post-top">
					<span class="single-post-date"><span class="glyphicon glyphicon-calendar"></span> <?php the_time('F jS, Y '); ?></span>
					<h1 class="single-post-heading"><?php the_title(); ?></h1>
					<div class="single-post-img">
						<img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/img/affiliate-summit.jpg" alt="">
					</div>
				</div>

				<div class="single-post-main">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>	
		
				
	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer();?>