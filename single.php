
<?php get_header() ; ?>

<div class="main-content container">
	<div class="wrapper">
		
		<?php 
			if ( has_category( 'post_news' ) || has_category( 'post_featured' )) {	
				get_template_part('single-news');
			} elseif ( has_category( 'post_gallery' ) ) {
				get_template_part('single-gallery');
			}
		?>
	</div> <!-- end wrapper -->
</div>

<?php get_footer() ; ?> 