<?php /* Template Name: Media */ ?>
	  

<?php get_header(); ?>

<div class="main-content container">
		<div class="row nomargin col-media-wrapper">
	
			<?php 
					$args = array( 'category_name' => 'post_gallery');
					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); 
			?>
					
			<div class="col-md-6 col-media wrapper nopadding" id="<?php the_ID(); ?>">
				<div class="col-media-featured-img">
					<?php 
						if(has_post_thumbnail) {
						the_post_thumbnail();
						} 
					?>
					<div class="col-media-mask">
						<?php 
							if(has_tag( 'video' )) {
								echo '<span class="glyphicon glyphicon-facetime-video folder-type"></span>';
							} else {
								echo '<span class="glyphicon glyphicon-camera folder-type"></span>';
							}
						?>	
						<a class="link-media-post" href="<?php the_permalink(); ?>">
							<span class="glyphicon glyphicon-link"></span>
						</a>
						<h3><?php the_title(); ?></h3>
					</div>
				</div>
				<div class="col-media-counter">
					<span class="col-media-count-item"><i>
					<?php 
						echo "#";	
					?>
					</i>items</span>
				</div>
			</div>
			
			<?php endforeach; 
				wp_reset_postdata();?>
			
		</div>
	</div> <!-- end main content -->

<?php get_footer(); ?>
