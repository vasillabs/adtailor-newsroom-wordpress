
<?php if(have_posts()) : while(have_posts()) : the_post() ?>
			<div class="single-post-wrapper" id="post-<?php the_ID(); ?>">
				<div class="single-post-top">
					<span class="single-post-date"><span class="glyphicon glyphicon-calendar"></span> <?php the_time('F jS, Y '); ?></span>
					<h1 class="single-post-heading"><?php the_title(); ?></h1>
					<div class="single-post-img">
						<?php 
							if(has_post_thumbnail) {
								the_post_thumbnail(); 
							} else {
							
							}
						?>
					</div>
				</div> <!-- end single post top -->
				
				<div class="single-post-main">
					<?php the_content(); ?>
				</div> <!-- end single post main --> 
			</div>
	<?php endwhile; ?>
<?php endif; ?>	
