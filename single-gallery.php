
	<div class="single-media-wrapper row">

		<?php if(have_posts()) : while(have_posts()) : the_post() ?>
		<div class="col-md-12 single-media-col gallery-post-<?php the_ID(); ?>">
			<div class="single-media-col-inner">
				<?php the_content(); ?>
			</div>
		</div>
		<?php endwhile; endif ?>
		
	</div>