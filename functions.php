<?php 

/*
*   Theme funcitons 
*   This file configures basic theme functions 
*   dynamic menus and sidebars
*/

/* register menu */

function register_adtailor_menus() {
	register_nav_menus(array(
		'top-menu' => __('Top Menu', 'adtailor'),
	));
}

add_action('init', 'register_adtailor_menus');


/* add class active to current menu link */
function active_nav_class($classes, $item) {
	if( in_array('current-menu-item' , $classes) ) {
		$classes[] = ' active';
	}
	return $classes;
}

add_filter('nav_menu_css_class' , 'active_nav_class' , 10 , 2);

/*  register JS and CSS files  */
function register_scripts() {
	wp_enqueue_style('bootstrap' , get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('theme-styles' , get_stylesheet_uri());
	
	wp_deregister_script('jquery');
	wp_register_script('jquery' , get_template_directory_uri() . '/js/jquery.min.js' , true);
	wp_register_script('bootstrap' , get_template_directory_uri() . '/js/bootstrap.min.js' , array('jquery'),'3.3.1' , true);
	wp_register_script('scripts' , get_template_directory_uri() . '/js/scripts.js' );
	
	// enque scripts 
	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap');
	wp_enqueue_script('scripts');
}

add_action('wp_enqueue_scripts','register_scripts');

// excerpt length add filter
function wpdocs_custom_excerpt_length($length) {
	return 15;
}

add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length',999 );

// add support for post thumbnails
add_theme_support( 'post-thumbnails' ); 
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );

// removes the thumbnail dimensions
function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}



// redirects to single media template 
/*add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
	if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
	return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);*/
?>
