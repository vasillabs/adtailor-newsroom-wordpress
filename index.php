
<?php /* Template Name: Home */ ?>
<?php get_header(); ?>


<div class="main-content container">
		<div class="slider">
			<div class="slider-post-img">	
				<?php 
					$args = array( 'posts_per_page' => 1,  'category_name' => 'post_featured' );
					$myposts = get_posts( $args );
					foreach( $myposts as $post ) : setup_postdata( $post ); 
				
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				} else {
					echo '<img class="img-responsive" src="' . get_bloginfo('stylesheet_directory') . '/img/affiliate-summit.jpg" alt="">';
				} ?>
				
			</div>
			<div class="slider-post-content row">
				<h2 class="slider-post-heading col-md-10">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h2>
				<span class="slider-post-date col-md-2 text-right"><?php the_time('F jS, Y'); ?></span>
				
				<?php endforeach;
					wp_reset_postdata(); ?>
			</div>
			<div class="top-stories-tag">
				<span>Featured News</span>
			</div>
		</div> <!-- end slider -->
		
		<div class="bottom-section row">
			<div class="bottom-section-left col-md-7">
				<h3>Recent News and Announcements</h3>

				<div class="featured-news">
					<?php 
					$args = array( 'posts_per_page' => 12, 'offset'=> 0, 'category_name' => 'post_news');
					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); 
					?>
					<div class="recent-post" id="<?php the_ID(); ?>">
						<span class="recent-post-date"> <?php the_time('F jS, Y '); ?></span>
						<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					</div>
					
					<?php endforeach; 
				wp_reset_postdata();?>
					
				</div>
				<a href="<?php the_field('home_page_link') ?>" class="link-news">see all</a>
			</div> <!--end bottom section left-->
			
			<div class="bottom-section-right col-md-5">
				<div class="contact-col">
					<h3>Contact us</h3>
					<a href="mailto:media@adtailor.com">media@adtailor.com</a>
				</div>
				
				<div class="events-calendar">
					<div class="events-calendar-headings row">
						<h3 class="col-md-6 upcomming-events-heading active"><span class="glyphicon glyphicon-triangle-right"></span>Upcoming events</h3>
						<h3 class="col-md-6 past-events-heading">Past events</h3>
					</div>
					
					<div class="events-calendar-wrapper">
					
						<div class="events-calendar-col upcomming-events">
							<div class="event">
								<p class="event-date">31 July - 2 August</p>
								<div class="event-description">
									<h4>Affiliate Summit East New York</h4>
									<p>Meet us at the Affiliate Summit in New York!</p>
								</div>
							</div>
						</div> <!-- end upcomming events wrapper -->
						
						<div class="events-calendar-col past-events">
							<div class="event">
								<p class="event-date">31 July - 2 August</p>
								<div class="event-description">
									<h4>Affiliate Summit East New York</h4>
									<p>Meet us at the Affiliate Summit in New York!</p>
								</div>
							</div>
						</div> <!-- end past events wrapper -->
						 
					</div>
				</div> <!-- end calendar wrapper -->
				
				<div class="social-media-buttons">
					<h3>Social Media</h3>
					<ul>
						<li class="linkedin">
							<a href="https://www.linkedin.com/company/adtailor" target="_blank">Linkedin</a>
						</li>
						<li class="facebook">
							<a href="https://www.facebook.com/adtailor/?fref=ts" target="_blank">Facebook</a>
						</li>
						<li class="twitter">
							<a href="https://twitter.com/adtailor" target="_blank">Twitter</a>
						</li>
					</ul>
				</div>
			</div><!--end bottom section right-->
		</div>
	</div> <!-- end main content -->

<?php get_footer();?>
